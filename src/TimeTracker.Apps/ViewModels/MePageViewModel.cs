﻿using MonkeyCache.FileStore;
using Newtonsoft.Json;
using Storm.Api.Dtos;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Windows.Input;
using TimeTracker.Apps.Pages;
using TimeTracker.Dtos;
using TimeTracker.Dtos.Accounts;
using TimeTracker.Dtos.Authentications;
using TimeTracker.Dtos.Authentications.Credentials;
using Xamarin.Forms;

namespace TimeTracker.Apps.ViewModels
{
    class MePageViewModel : ViewModelBase
    {
        private string email;
        public string Email { get => email; set => SetProperty(ref email, value); }

        private string firstName;
        public string FirstName { get => firstName; set => SetProperty(ref firstName, value); }

        private string lastName;
        public string LastName { get => lastName; set => SetProperty(ref lastName, value); }

        private string oldPassword;

        public string OldPassword { get => oldPassword; set => SetProperty(ref oldPassword, value); }

        private string newPassword;

        public string NewPassword { get => newPassword; set => SetProperty(ref newPassword, value); }

        public ICommand ChangePassword { get; }

        public ICommand GoToBackCommand { get; }

        public ICommand ChangeUser { get; }

        public ICommand BackToMeCommand { get; }
        public MePageViewModel()
        {
            ChangePassword = new Command(ChangerMDP);
            GoToBackCommand = new Command(BackCommand);
            BackToMeCommand = new Command(GoToMeCommand);
            ChangeUser = new Command(ChangerInfos);
        }

        private void ChangerInfos()
        {
            if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(lastName) || string.IsNullOrEmpty(firstName))
                App.Current.MainPage.DisplayAlert("Erreur", " Champ(s) non renseigné(s).", "ok");
            else
            {
                SubmitUser();
            }
        }

        private async void GoToMeCommand(object obj)
        {
            INavigationService navigationService = DependencyService.Get<INavigationService>();
            await navigationService.PushAsync<MenuMePage>();
        }

        private async void SubmitUser()
        {
            HttpClient client = new HttpClient();
            LoginResponse loginresponse = Barrel.Current.Get<LoginResponse>(key: "loginToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", loginresponse.AccessToken);
            SetUserProfileRequest setUserProfileRequest = ModifierUser();

            var method = new HttpMethod("PATCH");
            var request = new HttpRequestMessage(method, Urls.HOST + "/" + Urls.SET_USER_PROFILE)
            {
                Content = new StringContent(
                    JsonConvert.SerializeObject(setUserProfileRequest),
                    Encoding.UTF8, "application/json")
            };
            var response = await client.SendAsync(request);
            string res = await response.Content.ReadAsStringAsync();
            Response<LoginResponse> responsea = JsonConvert.DeserializeObject<Response<LoginResponse>>(res); // on convertie le json de la réponse en une classe

           

            
            if (response.IsSuccessStatusCode && responsea.IsSuccess)
            {
                LoginResponse loginResponse = responsea.Data;
                
                INavigationService nav = DependencyService.Get<INavigationService>();
                await nav.PushAsync<MePage>();
            }
            else
            {
                IDialogService dialogService = DependencyService.Get<IDialogService>();
                await dialogService.DisplayAlertAsync("Modification du compte echouée (" + responsea.ErrorCode + ")", responsea.ErrorMessage, "OK", "cancel");
            }
        }

        private SetUserProfileRequest ModifierUser()
        {
            SetUserProfileRequest setUserProfileRequest = new SetUserProfileRequest();
            setUserProfileRequest.Email = email;
            setUserProfileRequest.FirstName = firstName;
            setUserProfileRequest.LastName = lastName;
            return setUserProfileRequest;
        }

        private void BackCommand()
        {
            INavigationService navigationService = DependencyService.Get<INavigationService>();
            navigationService.PushAsync<MenuMePage>();
        }

        private void ChangerMDP()
        {
            if (string.IsNullOrEmpty(oldPassword) || string.IsNullOrEmpty(newPassword))
                App.Current.MainPage.DisplayAlert("Erreur", " Champ(s) non renseigné(s).", "ok");
            else
            {
                submit();
            }
        }

        private async void submit()
        {
            HttpClient client = new HttpClient();
            LoginResponse loginresponse = Barrel.Current.Get<LoginResponse>(key: "loginToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", loginresponse.AccessToken);
            SetPasswordRequest passwordRequest = modifierPass();


            //Test
            var method = new HttpMethod("PATCH");
            var request = new HttpRequestMessage(method, Urls.HOST + "/" + Urls.SET_PASSWORD)
            {
                Content = new StringContent(
                    JsonConvert.SerializeObject(passwordRequest),
                    Encoding.UTF8, "application/json")
            };
            var response = await client.SendAsync(request);
            

            // On récupère la réponse
            string res = await response.Content.ReadAsStringAsync();
            Response<LoginResponse> responsea = JsonConvert.DeserializeObject<Response<LoginResponse>>(res); // on convertie le json de la réponse en une classe

            Console.WriteLine("res = " + res);

            // Gère l'affichage pour dire si l'inscription s'est bien dérouler
            if (response.IsSuccessStatusCode && responsea.IsSuccess)
            {
                Console.WriteLine("Modification du mot de passe réussie!!");
                LoginResponse loginResponse = responsea.Data;
                IDialogService dialogService = DependencyService.Get<IDialogService>();
                await dialogService.DisplayAlertAsync("Modification du mot de passe réussie!", "Compte à jour", "OK", "cancel");

                INavigationService nav = DependencyService.Get<INavigationService>();
                await nav.PushAsync<MePage>();
            }
            else
            {
                Console.WriteLine("Modification du mot de passe echouée");

                IDialogService dialogService = DependencyService.Get<IDialogService>();
                await dialogService.DisplayAlertAsync("Modification du mot de passe echouée (" + responsea.ErrorCode + ")", responsea.ErrorMessage, "OK", "cancel");
            }

        }

        private SetPasswordRequest modifierPass()
        {
            SetPasswordRequest setPasswordRequest = new SetPasswordRequest();

            setPasswordRequest.NewPassword = newPassword;
            setPasswordRequest.OldPassword = oldPassword;
            return setPasswordRequest;
        }


    }
}
