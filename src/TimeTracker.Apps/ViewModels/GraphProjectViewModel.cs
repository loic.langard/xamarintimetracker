﻿using Microcharts;
using MonkeyCache.FileStore;
using SkiaSharp;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using TimeTracker.Apps.Pages;
using TimeTracker.Dtos.Projects;
using Xamarin.Forms;
using Entry = Microcharts.ChartEntry;

namespace TimeTracker.Apps.ViewModels
{
    class GraphProjectViewModel : ViewModelBase
    {
        public ICommand GoBackCommand { get; }
        public GraphProjectViewModel()
        {

            List<ProjectItem> projectItems = Barrel.Current.Get<List<ProjectItem>>(key: "ListProjectItem");
            int i = projectItems.Count;
            var random = new Random();
            var entries = new Entry[i];
            int j = -1;
            foreach (ProjectItem projet in projectItems)
            {
                j++;
                var color = String.Format("#{0:X6}", random.Next(0x1000000));
                int value = unchecked((int)projet.TotalSeconds);
                entries[j] = new Entry(value)
                {
                    Label = projet.Name,
                    ValueLabel = value.ToString(),
                    Color = SKColor.Parse(color)
                };

            }
            chartProject = new DonutChart { Entries = entries};
            GoBackCommand = new Command(GoToMenuProjectCommand);
        }

        private async void GoToMenuProjectCommand()
        {
            INavigationService nav = DependencyService.Get<INavigationService>();
            await nav.PushAsync<MenuProjectPage>();
        }

        private Chart chartProject;

        public Chart ChartProject { get => chartProject; set => SetProperty(ref chartProject, value); }



    }
}
