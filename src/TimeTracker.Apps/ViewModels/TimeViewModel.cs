﻿using MonkeyCache.FileStore;
using Newtonsoft.Json;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Windows.Input;
using TimeTracker.Apps.Pages;
using TimeTracker.Dtos;
using TimeTracker.Dtos.Authentications;
using TimeTracker.Dtos.Projects;
using Xamarin.Forms;

namespace TimeTracker.Apps.ViewModels
{
    class TimeViewModel : ViewModelBase
    {
        public ICommand StopCommand { get; }

        private DateTime debut;
        private DateTime fin;
        public TimeViewModel()
        {
            StopCommand = new Command(StopTimer);
            debut = DateTime.Now;

        }
        private async void StopTimer(object obj)
        {
            fin = DateTime.Now;
            AddTimeRequest addTimeRequest = NewAddTimeRequest();


            HttpClient client = new HttpClient();
            LoginResponse loginresponse = Barrel.Current.Get<LoginResponse>(key: "loginToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", loginresponse.AccessToken);
            long pid = Barrel.Current.Get<long>(key: "ProjectID");
            long tid = Barrel.Current.Get<long>(key: "TaskID");

            var json = JsonConvert.SerializeObject(addTimeRequest);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            
            var responseMessage = await client.PostAsync(Urls.HOST + "/" + Urls.ADD_TIME.Replace("{projectId}",pid.ToString()).Replace("{taskId}",tid.ToString()), data);

            if (responseMessage.IsSuccessStatusCode)
            {
                INavigationService nav = DependencyService.Get<INavigationService>();
                await nav.PushAsync<TaskPage>();
            }
            else
            {
                IDialogService dialogService = DependencyService.Get<IDialogService>();
                await dialogService.DisplayAlertAsync("ajout echouée (" + responseMessage.StatusCode + ")", "OK", "cancel");
            }
            

        }

        private AddTimeRequest NewAddTimeRequest()
        {
            AddTimeRequest addTimeRequest = new AddTimeRequest();
            addTimeRequest.EndTime = fin;
            addTimeRequest.StartTime = debut;
            return addTimeRequest;
        }
    }
}
