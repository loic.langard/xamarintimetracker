﻿using Storm.Mvvm;
using System.Windows.Input;

namespace TimeTracker.Apps.ViewModels
{
    public class Projet : NotifierBase
    {
        private long id;
        private string name;
        private string description;
        private long totalSeconds;

        public ICommand TaskCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand ModifyCommand { get; set; }

        public Projet(ICommand task, ICommand deleteCommand, ICommand modifyCommand)
        {
            TaskCommand = task;
            DeleteCommand = deleteCommand;
            ModifyCommand = modifyCommand;
        }

        public Projet(ICommand task, ICommand deleteCommand)
        {
            TaskCommand = task;
            DeleteCommand = deleteCommand;
        }



        public long Id
        {
            get => id;
            set => SetProperty(ref id, value);
        }

        public string Name
        {
            get => name;
            set => SetProperty(ref name, value);
        }

        public string Description
        {
            get => description;
            set => SetProperty(ref description, value);
        }
        
        public long TotalSeconds
        {
            get => totalSeconds;
            set => SetProperty(ref totalSeconds, value);
        }
    }
}