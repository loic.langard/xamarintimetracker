Projet Xamarin : TimeTracker


Notre application TimeTracker est exécutable sur un émulateur ou sur votre téléphone. 
Lors du lancement de celle-ci, il est possible de se connecter et de s'inscrire. Ensuite vous avez un menu qui vous laisse la possibilité de changer vos informations personnelles, consulter les projets et travailler.
Si vous décidez de changer vos informations personnelles, vous allez pouvoir changer votre email, nom, prénom et mot de passe.
Si vous choisissez de consulter les projets, vous allez voir dans un premier la liste des projets. Vous pourrez en ajouter ou consultez le graphique de tous les projets. Lors de la création d'un projet, vous pourrez lui donner un nom et une description. Vous avez également actions possibles pour un projet: voir ses tâches, le supprimer et le modifier. Si vous consultez les tâches d'un projet, vous allez voir la liste des tâches, vous pourrez en ajouter et voir le graphique du projet. Vous avez également la possibilité d'effectuer des actions sur une tâche: visionner les temps, la supprimer et la modifier. Si vous voulez visionner les temps, vous aurez la possibilité de supprimer et modifier les temps déjà ajoutés et vous pourrez en ajouter des nouveaux. Lors de l'ajout d'un nouveau temps, un timer se déclenche et il vous suffit de l'arrêter quand vous le souhaitez. Ce nouveau temps se rajoute donc dans la liste des temps de la tâche et le temps total de la tâche se met à jour.
Si vous souhaitez seulement travailler, vous avez là encore un timer qui se déclenche et que vous pouvez arrêter quand vous le voulez. Vous aurez ensuite la possibilité d'affecter ce temps au projet et à la tâche que vous désirez. 
Sur chaque page de l'application, il y a un bouton retour en haut au milieu qui permet de revenir sur la page précédente. 


NB: Nous avons développé ce projet sur le git mis à disposition par l'Université d'Orléans (Bitbucket), c'est pour cela qu'il n'y a que 2 commits sur gitlab. 
